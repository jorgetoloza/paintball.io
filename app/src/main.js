var id_cliente = null;

var paintball = new Kiwi.Game();
paintball.config = {
    width: 800,
    height: 600,
    minWidth: 10,
    minHeight: 10,
    maxWidth: 640,
    maxHeight: 450,
    speed: 5
};
var socket;
var state = new Kiwi.State('state', 800, 800);
state.preload = function () {
    Kiwi.State.prototype.preload.call(this);
    this.addSpriteSheet('morado', '../assets/img/spriteMorado.png', 150, 150);
    this.addSpriteSheet('verde', '../assets/img/spriteVerde.png', 150, 150);
    this.addSpriteSheet('municiones', '../assets/img/municiones.png', 50, 50);
    this.addImage("bala1", "../assets/img/bala1.png");
    this.addImage("bala2", "../assets/img/bala2.png");
    this.addImage("bala3", "../assets/img/bala3.png");
    this.addImage("bala4", "../assets/img/bala4.png");
};
state.create = function(){
 
    Kiwi.State.prototype.create.call(this);

    this.balas = new Kiwi.Group(this);
    this.jugadores = new Kiwi.Group(this);
    this.upKey = this.game.input.keyboard.addKey(Kiwi.Input.Keycodes.UP);
    this.leftKey = this.game.input.keyboard.addKey(Kiwi.Input.Keycodes.LEFT);
    this.downKey = this.game.input.keyboard.addKey(Kiwi.Input.Keycodes.DOWN);
    this.rightKey = this.game.input.keyboard.addKey(Kiwi.Input.Keycodes.RIGHT);  

    this.space = this.game.input.keyboard.addKey(Kiwi.Input.Keycodes.SPACEBAR);
    this.spaceUp = true;
    this.addChild(this.jugadores); 
    this.addChild(this.balas); 
    this.municiones = new Kiwi.GameObjects.Sprite(this, this.textures[ "municiones" ], 300, 220, false);
    this.municiones.animation.add("rotar", [0, 1, 2, 3], 0.5, true);
    this.municiones.animation.play('rotar');
    this.municiones.physics = this.components.add( new Kiwi.Components.ArcadePhysics( this.municiones, this.municiones.box ) );
    this.addChild(this.municiones);

    this.timer = paintball.time.clock.createTimer('time', 1, -1, false);
    this.timer.createTimerEvent(Kiwi.Time.TimerEvent.TIMER_COUNT, this.onTimerCount, this);
    this.timerCount = 0;
    this.timer.start();
};
state.onTimerCount = function () {
    this.timerCount += 1;
    // Cada 30 segundos cambiamos de posicion las municiones
    if(this.timerCount > 30){
        var x = Math.floor((Math.random() * (paintball.config.maxWidth - 150)) + paintball.config.minWidth);
        var y = Math.floor((Math.random() * (paintball.config.maxHeight - 150)) + paintball.config.minHeight);
        this.municiones.transform.x = x;
        this.municiones.transform.y = y;
        this.timerCount = 0;
    }
}
state.update = function() {
	Kiwi.State.prototype.update.call(this);
    if (this.upKey.isDown) {
        this.jugador.mover('up');
        socket.emit('moverJugador', {ID: this.jugador.ID, direccion: 'up'});
    }
    if (this.rightKey.isDown) {
        this.jugador.mover('right');
        socket.emit('moverJugador', {ID: this.jugador.ID, direccion: 'right'});
    }
    if (this.downKey.isDown) {
        this.jugador.mover('down');
        socket.emit('moverJugador', {ID: this.jugador.ID, direccion: 'down'});
    }
    if (this.leftKey.isDown) {
        this.jugador.mover('left');
        socket.emit('moverJugador', {ID: this.jugador.ID, direccion: 'left'});
    }
    if(this.space.isDown && this.spaceUp){ 
        // Si el espacio se presiono y ya lo solto para que haya un solo disparo por cada vez que se preciona
        this.jugador.disparar();
        var jugador = {};
        jugador.ID = this.jugador.ID;
        jugador.mira = this.jugador.mira;
        socket.emit('disparar', jugador);
        this.spaceUp = false;
        $(".jugador[id-jugador='"+state.jugador.ID+"'] span:last-child").text('Balas: '+state.jugador.balas);

    }
    if( this.space.isUp ){
        this.spaceUp = true;
    }
    this.checkCollisions();
};
var Bala = function(state, x, y, mira) {
    tipo = Math.floor((Math.random() * 4) + 1);
    Kiwi.GameObjects.Sprite.call(this, state, state.textures["bala"+tipo], x, y, false);
    this.physics = this.components.add( new Kiwi.Components.ArcadePhysics( this, this.box ) );
    this.speed = 8;
    this.mira = mira;
};
Kiwi.extend(Bala, Kiwi.GameObjects.Sprite);

Bala.prototype.update = function() {
    Kiwi.GameObjects.Sprite.prototype.update.call(this);
    if(this.mira == 'down')
        this.y += this.speed;
    if(this.mira == 'up')
        this.y -= this.speed;
    if(this.mira == 'right')
        this.x += this.speed;
    if(this.mira == 'left')
        this.x -= this.speed;
    
    if (this.x > paintball.config.width ||
            this.x < 0 ||
            this.y > paintball.config.height ||
            this.y < 0) {
        this.destroy();
    }
};
var Jugador = function(state, nombre, equipo, id, x, y) {
    this.ID = id;
    this.equipo = equipo;
    this.nombre = nombre;
    this.vida = 1;
    this.balas = 10;
    this.mira = 'up';
    Kiwi.GameObjects.Sprite.call(this, state, state.textures[this.equipo], x, y );
    this.physics = this.components.add( new Kiwi.Components.ArcadePhysics( this, this.box ) );
    this.animation.add("upRight", [0, 1, 2, 3], 0.02, false);
    this.animation.add("downRight", [4, 5, 6, 7], 0.02, false);
    this.animation.add("leftRight", [8, 9, 10, 11], 0.02, false);

    this.animation.add("upDown", [12, 13, 14, 15], 0.02, false);
    this.animation.add("rightDown", [16, 17, 18, 19], 0.02, false);
    this.animation.add("leftDown", [20, 21, 22, 23], 0.02, false);

    this.animation.add("upLeft", [24, 25, 26, 27], 0.02, false);
    this.animation.add("rightLeft", [28, 29, 30, 31], 0.02, false);
    this.animation.add("downLeft", [32, 33, 34, 35], 0.02, false);

    this.animation.add("leftUp", [36, 37, 38, 39], 0.02, false);
    this.animation.add("rightUp", [40, 41, 42, 43], 0.02, false);
    this.animation.add("downUp", [44, 45, 46, 47], 0.02, false);
    x += 57;
    y += 88;
    this.healthBar = new Kiwi.HUD.Widget.Bar(paintball, 30, 30, x, y, 30, 5, '#f5f5f5');
    this.healthBar.style.border = '1px solid #f5f5f5';
    this.healthBar._bar.style.transition = 'width .5s cubic-bezier(0,0,0.09,0.87)';

    this.inicial = new Kiwi.HUD.Widget.TextField(paintball, this.nombre.substring(0, 1), x+10, y-45);
    this.game.huds.defaultHUD.addWidget(this.inicial);

    this.inicial.style.color = '#f5f5f5';
    this.inicial.style.fontSize = '32px';
    this.inicial.style.fontFamily = 'Segoe UI';
    this.inicial.style.fontWeight = '300';
    this.herir = function(){
        this.vida -= 0.05;
        this.healthBar.counter.current -= 30 * 0.05;
    };
    this.disparar = function(){
        if(this.balas > 0){
            mira = this.mira;
            posx = this.x;
            posy = this.y;
            switch(mira){
                case 'left':
                    posy += 13;
                    posx += 25;
                break;
                case 'up':
                    posy += 6;
                    posx += 126;
                break;
                case 'right':
                    posy += 126;
                    posx += 68;
                break;
                case 'down':
                    posy += 75;
                    posx += 15;
                break;
            }
            var bala = new Bala(state, posx, posy, mira);
            bala.idJugador = this.ID;
            state.balas.addChild(bala);
            this.balas--;
        }
    };
    this.eliminar = function(id){
        socket.emit("eliminarJugador",id);
        $(".jugador[id-jugador='"+id+"']").remove();
        this.inicial.container.remove();
        this.inicial = 0;
        this.healthBar.container.remove();
        this.healthBar = null;
        this.destroy();
    }
    this.mover = function(direccion){
        switch(direccion){
            case 'up':
                if(this.mira != 'up'){
                    if(this.mira == 'left')
                        this.animation.play('leftUp');
                    if(this.mira == 'right')
                        this.animation.play('rightUp');
                    if(this.mira == 'down')
                        this.animation.play('downUp');

                    this.mira = 'up';
                }else{
                    if (this.transform.y > paintball.config.minHeight) {
                        this.transform.y -= paintball.config.speed;
                        this.healthBar.y = this.y + 88;
                        this.inicial.y = this.y + 41;
                    }
                }
            break;
            case 'right':
                if(this.mira != 'right'){
                    if(this.mira == 'up')
                        this.animation.play('upRight');
                    if(this.mira == 'down')
                        this.animation.play('downRight');
                    if(this.mira == 'left')
                        this.animation.play('leftRight');

                    this.mira = 'right';
                }else{
                    if (this.transform.x < paintball.config.maxWidth){
                        this.transform.x += paintball.config.speed;
                        this.healthBar.x = this.x + 57;
                        this.inicial.x = this.x + 67;
                    }
                }
            break;
            case 'down':
                if(this.mira != 'down'){
                    if(this.mira == 'up')
                        this.animation.play('upDown');
                    if(this.mira == 'right')
                        this.animation.play('rightDown');
                    if(this.mira == 'left')
                        this.animation.play('leftDown');

                    this.mira = 'down';
                }else{
                    if (this.transform.y < paintball.config.maxHeight) {
                        this.transform.y += paintball.config.speed;
                        this.healthBar.y = this.y + 88;
                        this.inicial.y = this.y + 41;
                    }
                }
            break;
            case 'left':
                if(this.mira != 'left'){
                    if(this.mira == 'up')
                        this.animation.play('upLeft');
                    if(this.mira == 'right')
                        this.animation.play('rightLeft');
                    if(this.mira == 'down')
                        this.animation.play('downLeft');

                    this.mira = 'left';
                }else{
                    if (this.transform.x > paintball.config.minWidth){
                        this.transform.x -= paintball.config.speed;
                        this.healthBar.x = this.x + 57;
                        this.inicial.x = this.x + 67;
                    }
                }
            break;
        }
    }
    paintball.huds.defaultHUD.addWidget(this.healthBar);
};
state.checkCollisions = function() {
    var i, j,
        balas = this.balas.members,
        jugadores = this.jugadores.members;

    for ( i = 0; i < balas.length; i++ ) {
        for ( j = 0; j < jugadores.length; j++ ) {
            if (balas[i].physics.overlaps(jugadores[j]) && balas[i].idJugador != jugadores[j].ID) {
                jugadores[j].herir();
                if(jugadores[j].vida <= 0){
                    jugadores[j].eliminar(jugadores[j].ID);
                } 
                balas[i].destroy();
                break;
            }
        }
    }
    for (var i = 0; i < jugadores.length; i++) {
        if (this.municiones.physics.overlaps(jugadores[i])){
            jugadores[i].balas += 10;
            var x = Math.floor((Math.random() * (paintball.config.maxWidth - 150)) + paintball.config.minWidth);
            var y = Math.floor((Math.random() * (paintball.config.maxHeight - 150)) + paintball.config.minHeight);
            this.municiones.transform.x = x;
            this.municiones.transform.y = y;
            $(".jugador[id-jugador='"+jugadores[i].ID+"'] span:last-child").text('Balas: '+jugadores[i].balas);

        }
    };
};

$(document).ready(function(){
    $('#welcome button').on('click', function(){
        socket = io.connect('172.16.8.43:9000');
        eventos();
        var nombre = $('#nombre').val();
        socket.emit('Ingresar', {nombre: nombre});
        socket.on('configurarJugador', function (jugador) {
            state.jugador = new Jugador(state, jugador.nombre, jugador.equipo, jugador.ID, jugador.x, jugador.y);
            state.jugadores.addChild(state.jugador);
            $("#jugadores").append('<div id-jugador="'+jugador.ID+'" class="jugador"><span>Nombre: '+state.jugador.nombre+'</span><span>Equipo: '+state.jugador.equipo+'</span><span>Balas: '+state.jugador.balas+'</span></div>');
            $("#jugadores").show();
            $('.container').css('top', '-100%');
            setTimeout(function(){
                $('.container input').val('');
                $('.container').remove();
                $('body > div:last-child').css('opacity', 1);
            }, 550);
        });
    });
});


function eventos(){
    socket.on('actualizarJugadores', function (jugadores) {
        for (var i = 0; i < jugadores.length; i++) {
            if(jugadores[i].ID != state.jugador.ID){
                jugador = jugadores[i];
                state.jugadores.addChild();
            }
        };
    });


    socket.on('crearJugador', function (jugador) {
        
        state.jugadores.addChild(new Jugador(state, jugador.nombre, jugador.equipo, jugador.ID, jugador.x, jugador.y));
        $("#jugadores").append('<div id-jugador="'+jugador.ID+'" class="jugador"><span>Nombre: '+jugador.nombre+'</span><span>Equipo: '+jugador.equipo+'</span><span>Balas: '+10+'</span></div>');
    });

    socket.on('moverJugador', function (data) {

            state.jugadores.members[getIndexJugador(data.ID)].mover(data.direccion);

    });

    socket.on('disparar', function (jugador) {
     
            state.jugadores.members[getIndexJugador(jugador.ID)].disparar();
            $(".jugador[id-jugador='"+jugador.ID+"'] span:last-child").text('Balas: '+state.jugadores.members[getIndexJugador(jugador.ID)].balas);

    });

    socket.on("idUsuario",function(id){
        id_cliente = id;
    });

    socket.on('eliminarJugador',function(d){
        if(d == id_cliente){
            socket.disconnect();
            location.reload();
        }
    })

    socket.on('crearJugadores', function (jugadores) {
        for (var i = 0; i < jugadores.length; i++) {
            if(jugadores[i].ID != state.jugador.ID && getIndexJugador(jugadores[i].ID) == false){
                jugador = jugadores[i];
                state.jugadores.addChild(new Jugador(state, jugador.nombre, jugador.equipo, jugador.ID, jugador.x, jugador.y));
                $("#jugadores").append('<div id-jugador="'+jugadores[i].ID+'" class="jugador"><span>Nombre: '+jugadores[i].nombre+'</span><span>Equipo: '+jugadores[i].equipo+'</span><span>Balas: 10</span></div>');
            }
        };
    });

    socket.on('ganador',function(d){
        if (state.jugador.equipo == d) {
            socket.disconnect();
            $("#ganaste").css('display',"flex");
            $("#ganaste h2").text("Gano el equipo: "+ d);
            $("#jugarDeNuevo").on('click',function(){
                location.reload();
            });
        }
    });

}

function getIndexJugador(id){
    for (var i = 0; i < state.jugadores.members.length; i++) {
        jugador = state.jugadores.members[i];
        if(jugador.ID == id)
            return i;
    };
    return false;
}
Kiwi.extend(Jugador, Kiwi.GameObjects.Sprite);
paintball.states.addState(state, true);
