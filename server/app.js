var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
app.use(express.static('../app'));

server.listen(9000,"172.16.8.43");

app.get('/', function (req, res) {
  res.sendfile(__dirname + '/index.html');
});

var jugadores = [];
var idClientes = 0;

io.on('connection', function (socket) {

	socket.on('Ingresar', function (data) {
    	var jugador = {};
    	jugador.nombre = data.nombre;
    	if(jugadores.length == 0)
	        jugador.equipo = 'morado';
	    else{
	        ultimoEquipo = jugadores[jugadores.length - 1].equipo;
	        jugador.equipo = ultimoEquipo == 'morado' ? 'verde' : 'morado';
	    }
	    x = Math.floor((Math.random() * (640 - 150)) + 10);
    	y = Math.floor((Math.random() * (450 - 150)) + 10);
    	jugador.x = x; 
    	jugador.y = y; 
	    jugador.ID = idClientes;
        socket.player = jugador;
        io.to(socket.id).emit('idUsuario',idClientes);
        
        idClientes = idClientes+1;

    	jugadores.push(jugador);

    	//Cuando ingresa un nuevo jugador le mandamos todos lo datos inicales para que lo configure en el cliente
    	socket.emit('configurarJugador', jugador);
    	//Cuando ingresa un nuevo jugador se lo mandamos a todos los clientes para que lo cree
    	socket.broadcast.emit('crearJugador', jugador);
    	//cuando ingresa un nuevo jugador entonces le mandamos todos los jugadores para que los cree
    	socket.emit('crearJugadores', jugadores);
	});

	socket.on('moverJugador', function (data) {
		socket.broadcast.emit('moverJugador', data);
	});

	socket.on('disparar', function (jugador) {
		socket.broadcast.emit('disparar', jugador);
	});
    
    socket.on('eliminarJugador',function(id){
        
        io.to(socket.id).emit('eliminarJugador',id);

    });

    socket.on('disconnect', function () {
        if(socket.player != null ){
            for(i = 0; i < jugadores.length; i++){
                if(jugadores[i].ID == socket.player.ID){
                     jugadores.splice(i,1);
                }
            }

            var morado = 0;
            var verde = 0;
            
            for(i = 0; i < jugadores.length;i++){
                if(jugadores[i].equipo == "morado"){
                    morado = morado +1;
                }else{
                    verde = verde +1;
                }
            }

            console.log("verde: " +verde)
            console.log("morado: "+morado)

            if (verde == 0 && morado > 0) {
                io.emit('ganador','morado');
            }

            if (verde > 0 && morado == 0) {
                io.emit('ganador','verde');
            }
        }
    });

});
